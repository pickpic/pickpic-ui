import { observer, inject } from 'mobx-react'
import { Link } from 'react-router-dom'
import hotkey from 'react-hotkey'
hotkey.activate()
@inject('store')
@observer
export default class Image extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      classification: props.store.allimages[props.match.params.id],
      id: props.match.params.id
    }
    this.set = async (id, e) => {
      const ix = _.keys(props.store.images).indexOf(id)
      const newimg = _.keys(props.store.images)[ix + 1]
      if (!newimg) {
        props.history.push('/')
      } else {
        props.history.push(`/${newimg}`)
      }
      await props.store.set(id, e)
    }
    this.keyHandler = e => {
      e.preventDefault()
      // Down key
      if (e.keyCode === 40) {
        this.set(this.state.id, 'unclassified')
      } else if (e.keyCode === 39) {
        // Right key
        this.set(this.state.id, 'good')
      } else if (e.keyCode === 37) {
        // Left key
        this.set(this.state.id, 'bad')
      }
    }
  }
  componentDidMount() {
    hotkey.addHandler(this.keyHandler)
  }
  componentWillUnmount() {
    hotkey.removeHandler(this.keyHandler)
  }
  componentWillReceiveProps(props) {
    this.setState({ classification: props.store.allimages[props.match.params.id], id: props.match.params.id })
  }
  render() {
    const store = this.props.store
    const id = this.props.match.params.id
    const classification = this.state.classification
    return (
      <div className="fill flex-column image animated fadeInDown">
        <Link className="home-button btn btn-lg btn-primary btn-action circle" to="/">
          <i className="fa fa-home fa-fw icon-only" />
        </Link>
        <img src={`/api/images/${id}`} />
        <div className="flex-row flex-center-align btn-group">
          {_.keys(store.classes).map(c => {
            return (
              <a className={`btn btn-lg ${classification === c ? 'active' : ''}`} key={`${c}_${classification}`} onClick={this.set.bind(this, id, c)}>
                <i className={`fa fa-${store.classes[c].icon} icon-only fa-fw`} />
              </a>
            )
          })}
        </div>
      </div>
    )
  }
}
