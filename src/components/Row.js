import { observer, inject } from 'mobx-react'
import Cell from './Cell'
@inject('store')
@observer
export default class Row extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  render() {
    return (
      <div className="row flex-row flex-center-align">
        {this.props.chunk.map(c => {
          return <Cell image={c} key={JSON.stringify(c)} />
        })}
      </div>
    )
  }
}
