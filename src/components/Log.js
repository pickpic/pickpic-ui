import { observer, inject } from 'mobx-react'
import { Link } from 'react-router-dom'
@inject('store')
@observer
export default class Log extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: false
    }
    this.revert = async () => {
      this.setState({ loading: true })
      await props.store.set(props.l.file, props.l.from)
      this.setState({ loading: false })
    }
  }

  render() {
    const l = this.props.l
    const store = this.props.store
    return (
      <div className="card flex-column animated zoomIn">
        <div className="card-header">
          <div className="card-subtitle text-gray">{l.file}</div>
        </div>
        {this.state.loading && <div className="fill flex-column loading" />}
        {!this.state.loading && (
          <div className="fill flex-column">
            <Link to={`/${l.file}`} className="card-image flex-center flex-column">
              <img src={`/api/thumbs/${l.file}`} className={l.classification} />
            </Link>
            <div className="card-footer flex-row flex-center">
              <div className="flex-row flex-center-align log-classification">
                <i className={`fa fa-fw icon-only fa-${store.classes[l.from].icon}`} />
                <i className="fa fa-fw icon-only fa-arrow-right" />
                <i className={`fa fa-fw icon-only fa-${store.classes[l.classification].icon}`} />
              </div>
              <div className="btn-group flex-row flex-center-align">
                <a className="btn btn-primary btn-sm" onClick={this.revert.bind(this, l.from)}>
                  <i className="fa fa-fw fa-undo icon-only" />
                </a>
              </div>
            </div>
          </div>
        )}
      </div>
    )
  }
}
