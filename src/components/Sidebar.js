import { observer, inject } from 'mobx-react'
import Log from './Log'
import { toJS } from 'mobx'
@inject('store')
@observer
export default class Sidebar extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
    this.filterToggle = e => {
      props.store.filterToggle(e)
    }
    this.clearLog = () => {
      props.store.log = []
    }
  }
  render() {
    const store = this.props.store
    const classed = _.countBy(_.values(store.allimages))
    const log = _.reverse(toJS(store.log))
    return (
      <div className="sidebar flex-column">
        <section>
          <div className="flex-row flex-center-align title">
            <h5>
              <i className="fa fa-filter" />
              Filters
            </h5>
          </div>
          <div className="btn-group btn-group-block flex-row flex-center-align">
            {_.keys(store.classes).map((c, i) => {
              return (
                <a
                  key={`${c}_${classed[c]}`}
                  className={`btn fill flex-row flex-center btn-${store.classes[c].class} ${store.filter.indexOf(c) > -1 ? 'active' : ''}`}
                  onClick={this.filterToggle.bind(this, c)}
                >
                  <i className={`fa fa-${store.classes[c].icon}`} />
                  <span className="flex-row">{classed[c] || 0}</span>
                </a>
              )
            })}
          </div>
        </section>
        <section className="fill flex-column">
          <div className="flex-row flex-center-align title">
            <h5 className="flex-row flex-center-align fill">
              <i className="fa fa-clock-o" />
              Recent
            </h5>
            <a className="btn btn-primary btn-sm" onClick={this.clearLog}>
              <i className="fa fa-refresh" />
              Clear
            </a>
          </div>
          <div className="log scroll">
            {log.map(l => {
              return <Log key={JSON.stringify(l)} l={l} />
            })}
          </div>
        </section>
      </div>
    )
  }
}
