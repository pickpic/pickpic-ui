import { observer, inject } from 'mobx-react'
import Row from './Row'
import Sensor from 'react-visibility-sensor'
@inject('store')
@observer
export default class Grid extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      rows: 8
    }
    this.addRow = () => {
      this.setState({ rows: this.state.rows + 3 })
    }
  }
  componentDidMount() {
    this.setState({ width: this.refs.grid.offsetWidth, height: this.refs.grid.clientHeight, rows: this.refs.grid.clientHeight / 200 + 3 })
  }
  render() {
    const store = this.props.store
    let chunks
    if (this.state.width) {
      const num = this.state.width / 200
      const imgs = []
      _.keys(store.images).map(img => imgs.push({ file: img, classification: store.images[img] }))
      chunks = _.take(_.chunk(imgs, num), this.state.rows)
    }
    return (
      <div className="fill flex-column overview" ref="grid">
        {this.state.width && (
          <div className="fill flex-column">
            {chunks.map(c => {
              return <Row key={JSON.stringify(c)} chunk={c} />
            })}
            <Sensor onChange={this.addRow} offset={{ top: 300 }} />
          </div>
        )}
      </div>
    )
  }
}
