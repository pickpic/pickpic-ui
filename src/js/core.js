import '../css/style.sass'
import ReactDOM from 'react-dom'
import App from '../components/App'
import Store from '../store'
import { AppContainer } from 'react-hot-loader'
import { Provider } from 'mobx-react'
import WebFont from 'webfontloader'
const store = window.store || new Store()
window.store = store
const render = async () => {
  await store.load()
  ReactDOM.render(
    <AppContainer>
      <Provider store={store}>
        <App />
      </Provider>
    </AppContainer>,
    document.getElementById('main')
  )
}
WebFont.load({
  google: {
    families: ['Open Sans:200,300,400,600']
  },
  active: () => {
    render(store)
  }
})
if (module.hot) {
  module.hot.accept(() => render(store))
}
