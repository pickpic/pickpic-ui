import { observer, inject } from 'mobx-react'
import { Link } from 'react-router-dom'
@inject('store')
@observer
export default class Cell extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
    this.set = e => {
      props.store.set(this.props.image.file, e)
    }
  }
  render() {
    const l = this.props.image
    const store = this.props.store
    return (
      <div className="card flex-column animated fadeIn">
        <div className="card-header">
          <div className="card-subtitle text-gray">{l.file}</div>
        </div>
        <Link to={`/${l.file}`} className="card-image flex-center flex-column">
          <img src={`/api/thumbs/${l.file}`} className={`${l.classification} ${this.state.loaded ? '' : 'loading'}`} />
        </Link>
        <div className="card-footer flex-row flex-center">
          <div className="btn-group flex-row flex-center-align">
            {_.keys(store.classes).map(c => {
              return (
                <a key={c} className={`btn btn-sm ${l.classification === c ? 'active' : ''}`} onClick={this.set.bind(this, c)}>
                  <i className={`fa fa-fw fa-${store.classes[c].icon} icon-only`} />
                </a>
              )
            })}
          </div>
        </div>
      </div>
    )
  }
}
