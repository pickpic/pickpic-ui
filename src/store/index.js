import { observable, action } from 'mobx'
import faye from 'faye'
export default class {
  @observable loading = true
  @observable images
  @observable log
  classes = {
    bad: { icon: 'thumbs-down', class: 'danger' },
    unclassified: { icon: 'question', class: 'info' },
    good: { icon: 'thumbs-up', class: 'success' }
  }
  @observable filter = ['unclassified']
  @action
  filterToggle(e) {
    if (this.filter.indexOf(e) > -1) {
      this.filter.remove(e)
    } else {
      this.filter.push(e)
    }
    if (this.filter.length === 0) {
      this.filter = ['unclassified']
    }
    this.images = _.pickBy(this.allimages, im => this.filter.indexOf(im) > -1)
  }
  @action
  async set(id, classification) {
    await axios.get(`/api/set/${id}/${classification}`)
  }
  @action
  async load() {
    this.loading = true
    this.fc = new faye.Client('/api/faye')
    this.fc.on('transport:up', async () => {
      if (!this.changessub) {
        this.changessub = this.fc.subscribe('/changes', async message => {
          this.log = await axios.get('/api/log').then(d => d.data)
          this.allimages[message.file] = message.classification
          this.images = _.pickBy(this.allimages, im => this.filter.indexOf(im) > -1)
        })
      }
      this.fc.subscribe('/initial', message => {
        this.images = message
        this.allimages = message
        this.images = _.pickBy(this.allimages, im => this.filter.indexOf(im) > -1)
      })
      this.log = await axios.get('/api/log').then(d => d.data)
      this.allimages = await axios.get('/api/list').then(d => d.data)
      this.images = _.pickBy(this.allimages, im => this.filter.indexOf(im) > -1)
      this.loading = false
    })
    this.fc.connect()
  }
}
