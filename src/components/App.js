import { observer, inject } from 'mobx-react'
import { Route } from 'react-router-dom'
import Sidebar from './Sidebar'
import Grid from './Grid'
import Image from './Image'
import { BrowserRouter } from 'react-router-dom'
@inject('store')
@observer
export default class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  render() {
    const store = this.props.store
    if (store.loading) {
      return <div className="fill flex-column loading" />
    } else {
      return (
        <BrowserRouter>
          <div className="fill flex-row">
            <Sidebar />
            <Route exact path="/" component={Grid} />
            <Route path="/:id" component={Image} />
          </div>
        </BrowserRouter>
      )
    }
  }
}
